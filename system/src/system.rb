require_relative 'redis_collection'
require 'sinatra/cross_origin'
require 'sinatra/base'
require 'json'

$stdout.sync = true

class System < Sinatra::Base
  @@collection = RedisCollection.new

  set :show_exceptions, false
  set :raise_errors, true

  configure do
    set :logging, true
    enable :cross_origin
  end

  before do
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = ['OPTIONS', 'PATCH', 'POST', 'GET']

    content_type :json
  end

  options '*' do
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type'
  end

  post '/store-message/:chat' do
    chat = params["chat"]
    body = JSON.parse(request.body.read)
    text = body["message"]

    message = { "message" => text, "chat" => chat }

    @@collection.create(chat) unless @@collection.exists?(chat)
    @@collection.remove_last_item(chat) if @@collection.size(chat) >= 50
    @@collection.store(text, chat)

    message.to_json
  end

  get '/retrieve-messages/:chat' do
    chat_name = params["chat"]

    @@collection.create(chat_name) unless @@collection.exists?(chat_name)
    messages = @@collection.retrieve(chat_name)
    chat = { "chat" => chat_name, "messages" => messages }

    chat.to_json
  end
end
