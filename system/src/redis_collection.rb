require 'redis'
require 'json'

class RedisCollection
  def initialize
    @collection = Redis.new(url: ENV['REDIS_URL'])
  end

  def create(name)
    empty_items = []

    set(empty_items, name)
  end

  def store(item, name)
    items = get(name)

    items.push(item)

    set(items, name)
  end

  def retrieve(name)
    get(name)
  end

  def remove_last_item(name)
    items = get(name)

    items.shift

    set(items, name)
  end

  def size(name)
    get(name).size
  end

  def exists?(name)
    !!collection.get(name)
  end

  def reset
    collection.flushdb
  end

  private

  def get(name)
    value = collection.get(name)

    JSON.parse(value)
  end

  def set(value, name)
    collection.set(name, value.to_json)
  end

  def collection
    @collection
  end
end
