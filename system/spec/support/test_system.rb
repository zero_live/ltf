require 'net/http'
require 'redis'
require 'json'

class TestSystem
  class << self
    SYSTEM_URL = 'http://127.0.0.1:8181'

    def send(path, params)
      response = post(path, params)

      body(response)
    end

    def retrieve(path)
      response = get(path)

      body(response)
    end

    def reset
      Redis.new(url: 'redis://redis:6379').flushdb
    end

    private

    def get(path)
      uri = uri(path)

      response = Net::HTTP.get_response(uri)

      response
    end

    def post(path, params)
      uri = uri(path)

      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri)
      request.body = params.to_json
      response = http.request(request)

      response
    end

    def body(response)
      JSON.parse(response.body)
    end

    def uri(path)
      URI(SYSTEM_URL + path)
    end
  end
end
