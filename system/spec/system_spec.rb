require 'support/test_system'

describe 'System' do
  before(:each) do
    TestSystem.reset
  end

  it 'stores messages' do
    chat = 'chat_name'
    message = 'Some message here'

    response = TestSystem.send('/store-message/' + chat, { message: message })

    expect(response).to eq({ "message" => message, "chat" => chat })
  end

  it 'retrieves stores messages' do
    chat = 'chat_name'
    message = 'Some message here'
    TestSystem.send('/store-message/' + chat, { "message" => message })
    TestSystem.send('/store-message/' + chat, { "message" => message })

    response = TestSystem.retrieve('/retrieve-messages/' + chat)

    expect(response).to eq({ "messages" => [ message, message ], "chat" => chat })
  end

  it 'stores 50 newest messages' do
    chat = 'chat_name'
    newest_message = 'Newest message'
    51.times { TestSystem.send('/store-message/' + chat, { "message" => 'Some message here' }) }
    TestSystem.send('/store-message/' + chat, { "message" => newest_message })

    response = TestSystem.retrieve('/retrieve-messages/' + chat)

    messages = response["messages"]
    expect(messages.size).to eq(50)
    expect(messages.last).to eq(newest_message)
  end

  it 'retrieves new chat for unexistent chat' do
    chat = 'chat_name'

    response = TestSystem.retrieve('/retrieve-messages/' + chat)

    expect(response).to eq({ "messages" => [], "chat" => chat })
  end
end
