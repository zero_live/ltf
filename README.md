# LFT

A non-memory web chat

## System requirements

- `docker-compose version 1.24.1` or higher.
- `Docker version 19.03.5` or higher.

## How to run the project

Run the following command in the project folder:

- `sh scripts/up`

## How to tun the tests

After run the project you have to run the following command:

- `sh scripts/test`
