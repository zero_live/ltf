import { HttpRequester } from "../utilities/HttpRequester.js"
import { Config } from "../utilities/Config.js"

export class Actions {
  constructor() {
    this.storeMessage = this.storeMessage.bind(this)
    this.retrieveMessages = this.retrieveMessages.bind(this)
  }

  storeMessage({ message, chat }, callback) {
    const fullUrl = Config.systemUrl() + '/store-message/' + chat

    HttpRequester.post(fullUrl, callback, { message })
  }

  retrieveMessages({ chat }, callback) {
    const fullUrl = Config.systemUrl() + '/retrieve-messages/' + chat

    HttpRequester.get(fullUrl, callback)
  }
}
