const XHR_STATE_COMPLETED = 4

export class HttpRequester {
  static post(url, callback, payload = {}) {
    this._hit('POST', url, callback, payload)
  }

  static get(url, callback, payload = {}) {
    this._hit('GET', url, callback, payload)
  }

  static patch(url, callback, payload) {
    this._hit('PATCH', url, callback, payload)
  }

  static delete(url, callback, payload = {}) {
    this._hit('DELETE', url, callback, payload)
  }

  static _hit(method, url, callback, payload = {}) {
    const xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {
      const hasStatusCode = (this.status !== 0)

      if (this.readyState === XHR_STATE_COMPLETED && hasStatusCode) {
        const response = JSON.parse(this.responseText)

        callback(response)
      }
    }

    xhttp.open(method, url, true)
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.send(this._adapt(payload))
  }

  static put(url, callback, payload) {
    this._hitWithoutAdapt('PUT', url, callback, payload)
  }

  static _hitWithoutAdapt(method, url, callback, payload = {}) {
    const xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function() {
      const hasStatusCode = (this.status !== 0)

      if (this.readyState === XHR_STATE_COMPLETED && hasStatusCode) {
        const response = JSON.parse(this.responseText)

        callback(response)
      }
    }

    xhttp.open(method, url, true)
    xhttp.setRequestHeader('Content-Type', 'application/json')
    xhttp.send(JSON.stringify(payload))
  }

  static _adapt(payload) {
    return JSON.stringify(payload)
  }
}
