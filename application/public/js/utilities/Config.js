
export class Config {
  static systemUrl() {
    let url = 'http://127.0.0.1:8181'

    if (window.location.href.includes('application')) { url = 'http://system:8181' }
    if (window.location.href.includes('gitlab')) { url = 'https://ltf-system.herokuapp.com' }

    return url
  }

  static applicationUrl() {
    let url = 'http://localhost:8080'

    if (window.location.href.includes('application')) { url = 'http://application:8080' }
    if (window.location.href.includes('gitlab')) { url = 'https://zero_live.gitlab.io/ltf' }

    return url
  }
}
