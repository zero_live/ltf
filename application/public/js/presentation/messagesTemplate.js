
export const messagesTemplate = (messages) => {
  const whiteMessage = (text) => {
    return `
      <p class="custom-little-padding is-size-5">
        ${text}
      </p>
    `
  }

  const blackMessage = (text) => {
    return `
      <p class="has-background-grey-lighter custom-little-padding is-size-5">
        ${text}
      </p>
    `
  }

  const isOdd = (number) => { return (number % 2 === 0) }

  const lines = messages.map((message, index) => {
    let line = blackMessage(message)

    if (isOdd(index)) { line = whiteMessage(message) }

    return line
  })

  const html = lines.join('')
  return html
}
