import { generateUuidv4 } from '../utilities/generateUuidv4.js'
import { messagesTemplate } from "./messagesTemplate.js"
import { Actions } from "../domain/Actions.js"
import { Config } from '../utilities/Config.js'

const NEW_CHAT_LINK_ID = 'new-chat-link'
const NEW_CHAT_BUTTON_ID = 'new-chat-button'
const INPUT_ID = 'message-text-input'
const SEND_BUTTON_ID = 'send-message-button'
const DEFAULT_CHAT_NAME= 'DEFAULT'

export class LtfPage {
  constructor() {
    this.actions = new Actions()
    this.messages = []

    this.askMessages()
    this.keepMessagesUpdated()
  }

  askMessages() {
    this.actions.retrieveMessages({ chat: this.chatName() }, this.updateMessages.bind(this))
  }

  sendMessageWithEnter(event) {
    if (!this.isEnter(event)) { return }

    this.sendMessage()
  }

  sendMessage() {
    if (this.hasEmptyMessage()) { return }
    const message = this.message()
    const chat = this.chatName()

    this.cleanMessage()
    this.actions.storeMessage({ message, chat }, this.askMessages.bind(this))
  }

  draw() {
    const currentMessage = this.message()

    this.addTemplateToHtml()

    this.addMessageText(currentMessage)

    this.addCallbacks()
    this.goToBottom()
  }

  //private

  addMessageText(message) {
    this.messageTextInput().value = message
  }

  keepMessagesUpdated() {
    clearInterval(this.askMessages.bind(this))
    setInterval(this.askMessages.bind(this), 5000)
  }

  addTemplateToHtml() {
    const page = document.getElementById('page')
    page.innerHTML = this.template()
  }

  message() {
    const input = this.messageTextInput()

    return input.value
  }

  cleanMessage() {
    const input = this.messageTextInput()

    input.value = ''
  }

  updateMessages({ messages }) {
    this.messages = messages

    this.draw()
  }

  template() {
    return `
      <nav class="navbar is-dark is-fixed-top" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
          <a class="navbar-item">
            <span id="title" class="is-size-5">LTF</span>
          </a>

          <a id="${NEW_CHAT_BUTTON_ID}" role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
            <span aria-hidden="true"><i class="fas fa-plus-circle"></i></span>
          </a>

          <a style="display: none;" role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
            <span aria-hidden="true"><i class="fas fa-share-alt"></i></span>
          </a>
        </div>

        <div class="navbar-menu">
          <div class="navbar-menu">
            <div class="navbar-end">
              <a id="${NEW_CHAT_LINK_ID}" class="navbar-item">
                <i class="fas fa-plus-circle"></i>
              </a>
              <a class="navbar-item">
                <i style="display: none;" class="fas fa-share-alt"></i>
              </a>
            </div>
          </div>
        </div>
      </nav>

      <section class="section is-paddingless custom-fill-body">
        <div class="container custom-one-rem-padding">

          ${this.messagesTemplate()}

        </div>
      </section>

      <footer class="footer is-paddingless custom-footer-color">
        <div class="content custom-one-rem-padding">
          <p>
            <div class="field is-grouped">
              <p class="control is-expanded">
                <input id="${INPUT_ID}" class="input is-rounded" type="text">
              </p>
              <p class="control">
                <a id="${SEND_BUTTON_ID}" class="button is-info is-rounded">
                  <i class="fas fa-paper-plane"></i>
                </a>
              </p>
            </div>
          </p>
        </div>
      </footer>
    `
  }

  messagesTemplate() {
    return messagesTemplate(this.messages)
  }

  goToBottom() {
    const input = this.messageTextInput()

    input.focus()
  }

  openNewChat() {
    const chatName = this.randomChatName()

    window.open(Config.applicationUrl() + chatName , '_blank')
  }

  randomChatName() {
    const chatName = generateUuidv4()

    return chatName
  }

  messageTextInput() {
    const emptyInput = { value: '' }
    const input = document.getElementById(INPUT_ID) || emptyInput

    return input
  }

  chatName() {
    let chatName = window.location.hash

    if(!chatName) { chatName = DEFAULT_CHAT_NAME }
    return chatName.replace('#', '')
  }

  addCallbacks() {
    this.addOnClickListener(SEND_BUTTON_ID, this.sendMessage.bind(this))
    this.addKeyPressListener(INPUT_ID, this.sendMessageWithEnter.bind(this))
    this.addOnClickListener(NEW_CHAT_BUTTON_ID, this.openNewChat.bind(this))
    this.addOnClickListener(NEW_CHAT_LINK_ID, this.openNewChat.bind(this))
  }

  addOnClickListener(id, callback) {
    document.getElementById(id).addEventListener('click', callback)
  }

  addKeyPressListener(id, callback) {
    document.getElementById(id).addEventListener('keypress', callback)
  }

  hasEmptyMessage() {
    return (this.message() === '')
  }

  isEnter(event) {
    return (event.keyCode === 13)
  }
}
