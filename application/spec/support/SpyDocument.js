
export class SpyDocument {
  constructor() {
    global.document = this
    this.elements = {}
  }

  getElementById(id) {
    if (this.elements[id]) { return this.elements[id] }
    const element = new StubElement()

    this.elements[id] = element

    return element
  }

  fill(id, value) {
    this.elements[id].fill(value)
  }

  click(id) {
    this.elements[id].click()
  }

  press(code, id) {
    this.elements[id].keyPress({ keyCode: code })
  }

  content() {
    const ids = Object.keys(this.elements)
    const templates = ids.map(id => this.elements[id].template())
    const html = templates.join('')

    return html
  }
}

class StubElement {
  constructor() {
    this.innerHTML = ''
    this.listeners = {}
    this.value
  }

  addEventListener(event, callback) {
    this.listeners[event] = callback
  }

  focus() {}
  blur() {}

  fill(value) {
    this.value = value
  }

  click() {
    this.listeners.click()
  }

  keyPress(event) {
    this.listeners.keypress(event)
  }

  template() {
    return this.innerHTML
  }

  hasEmptyValue() {
    return (this.value === '')
  }
}
