
export class StubLocation {
  static withDevelopment() {
    this.href()
  }

  static href(url='') {
    global.window = { location: { href: url } }
  }

  static hash(string) {
    global.window = { location: { href: '', hash: '#' + string } }
  }

  static restore() {
    global.window = undefined
  }
}
