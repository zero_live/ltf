import { LtfPage } from "../../public/js/presentation/LftPage"
import { SpyDocument } from "./SpyDocument"

export class TestLtfPage {
  withStubbedUuid(uuid) {
    const testPage = new TestLtfPage()

    testPage.page.randomChatName = () => { return uuid }

    return testPage
  }

  constructor() {
    this.spyDocument = new SpyDocument()
    this.page = new LtfPage()
  }

  draw() {
    this.page.draw()

    return this
  }

  disableAskMessages() {
    this.page.askMessages = function() {}
  }

  fillMessage(text) {
    this.spyDocument.fill('message-text-input', text)
  }

  sendMessage() {
    this.spyDocument.click('send-message-button')
  }

  pressEnterAtMessage() {
    const enterCode = 13
    this.spyDocument.press(enterCode, 'message-text-input')
  }

  clickNewChatButton() {
    this.spyDocument.click('new-chat-button')
  }

  content() {
    return this.spyDocument.content()
  }

  hasMessageInputEmpty() {
    const input = this.spyDocument.getElementById('message-text-input')

    return input.hasEmptyValue()
  }
}
