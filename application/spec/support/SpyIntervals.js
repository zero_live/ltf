
export class SpyIntervals {
  constructor() {
    this.callback
    this.interval
    this._cleanedCallback

    global.setInterval = this.setInterval.bind(this)
    global.clearInterval = this.clearInterval.bind(this)
  }

  setInterval(callback, interval) {
    this.callback = callback
    this.interval = interval
  }

  clearInterval(callback) {
    this._cleanedCallback = callback
  }

  executesCallbackEvery() {
    return this.interval
  }

  executes() {
    return this.callback.name
  }

  cleanedCallback() {
    return this._cleanedCallback.name
  }

  restore() {
    global.setInterval = undefined
  }
}
