
export class SpyWindow {
  constructor() {
    this.url
    this.option

    if (!window) { global.window = {} }
    window.open = this.open.bind(this)
  }

  open(url, option) {
    this.url = url
    this.option = option
  }

  restore() {
    global.window = undefined
  }

  hasOpenAnotherTab() {
    return (this.option === '_blank')
  }

  includesUrlOpenedTab(string) {
    return (this.url.includes(string))
  }
}
