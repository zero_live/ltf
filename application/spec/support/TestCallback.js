export class TestCallback {
  constructor () {
    this.message = null
    this.isCalled = false

    this.execute = this.execute.bind(this)
  }

  execute (message) {
    this.message = message
    this.isCalled = true
  }

  hasBeenCalledWith () {
    return this.message
  }

  hasBeenCalled () {
    return this.isCalled
  }
}
