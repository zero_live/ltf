export class HttpSpy {
  static enableWith (response) {
    const spy = new HttpSpy(response)

    global.XMLHttpRequest = () => { return spy }

    return spy
  }

  constructor (response) {
    this.method = undefined
    this.endpoint = undefined
    this.payload = undefined
    this.response = response
    this.readyState = 4
    this.status = 200
    this.responseText = JSON.stringify(this.response)
    this.onreadystatechange = () => {}
  }

  open(method, endpoint) {
    this.endpoint = endpoint
    this.method = method
  }

  setRequestHeader() {}

  send(payload='{}') {
    this.onreadystatechange()
    this.payload = JSON.parse(payload)
  }

  hasBeenCalled() {
    return (this.payload != undefined)
  }

  hasBeenCalledWithMethod() {
    return this.method
  }

  hasBeenCalledWithUrl() {
    return this.endpoint
  }

  hasBeenCalledWithPayload() {
    return this.payload
  }

  disable() {
    global.XMLHttpRequest = undefined
  }
}
