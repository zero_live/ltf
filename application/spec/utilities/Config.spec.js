import { Config } from "../../public/js/utilities/Config"
import { StubLocation } from "../support/StubLocation"

describe('Config', () => {

  beforeEach(() => {
    StubLocation.href()
  })

  afterEach(() => {
    StubLocation.restore()
  })

  it('retrieves the URL of the development System', () => {
    StubLocation.withDevelopment()
    const developmentUrl = 'http://127.0.0.1:8181'

    const url = Config.systemUrl()

    expect(url).toEqual(developmentUrl)
  })

  it('retrieves the URL of the e2e System', () => {
    StubLocation.href('http://application:8080')
    const endToEndUrl = 'http://system:8181'

    const url = Config.systemUrl()

    expect(url).toEqual(endToEndUrl)
  })

  it('retrieves the URL of the demo System', () => {
    StubLocation.href('https://zero_live.gitlab.io/ltf/')
    const demoUrl = 'https://ltf-system.herokuapp.com'

    const url = Config.systemUrl()

    expect(url).toEqual(demoUrl)
  })
})
