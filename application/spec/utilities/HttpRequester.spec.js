import { HttpRequester } from '../../public/js/utilities/HttpRequester'
import { TestCallback } from '../support/TestCallback'
import { HttpSpy } from '../support/HttpSpy'


describe('Http Client', () => {
  const payload = { anyKey: 'payload' }
  const url = 'http://specified.url/'
  let callback = undefined
  let spy = undefined

  beforeEach(() => {
    callback = new TestCallback()
    spy = HttpSpy.enableWith(payload)
  })

  afterEach(() => {
    spy.disable()
  })

  it('sends POST with a payload to specified URL', () => {

    HttpRequester.post(url, callback.execute, payload)

    expect(spy.hasBeenCalledWithUrl()).toEqual(url)
    expect(spy.hasBeenCalledWithMethod()).toEqual('POST')
    expect(spy.hasBeenCalledWithPayload()).toEqual(payload)
    expect(callback.hasBeenCalledWith()).toEqual(payload)
  })

  it('sends GET with a payload to specified URL', () => {

    HttpRequester.get(url, callback.execute, payload)

    expect(spy.hasBeenCalledWithUrl()).toEqual(url)
    expect(spy.hasBeenCalledWithMethod()).toEqual('GET')
    expect(spy.hasBeenCalledWithPayload()).toEqual(payload)
    expect(callback.hasBeenCalledWith()).toEqual(payload)
  })
})
