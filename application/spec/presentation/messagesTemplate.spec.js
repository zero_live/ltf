import { messagesTemplate } from "../../public/js/presentation/messagesTemplate"

describe('messagesTemplate', () => {
  it('draws nothing when there are no messages', () => {
    const noMessages = []

    const template = messagesTemplate(noMessages)

    expect(template).toEqual('')
  })

  it('draws a non-grey message', () => {
    const message = 'Some message text'
    const messages = [message]

    const template = messagesTemplate(messages)

    expect(template).toContain(message)
    expect(template).not.toContain('grey')
  })

  it('draws greys messages', () => {
    const someMessage = 'Some message text'
    const anotherMessage = 'Another message text'
    const messages = [someMessage, anotherMessage]

    const template = messagesTemplate(messages)

    expect(template).toContain(someMessage)
    expect(template).toContain(anotherMessage)
    expect(template).toContain('grey')
  })
})
