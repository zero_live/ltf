import { StubLocation } from "../support/StubLocation"
import { SpyIntervals } from "../support/SpyIntervals"
import { TestLtfPage } from "../support/TestLtfPage"
import { SpyWindow } from "../support/SpyWindow"
import { HttpSpy } from "../support/HttpSpy"

describe('LftPage', () => {
  let httpSpy, spyIntervals

  beforeEach(() => {
    spyIntervals = new SpyIntervals()
    StubLocation.withDevelopment()
    httpSpy = HttpSpy.enableWith(null)
  })

  afterEach(() => {
    httpSpy.disable()
    StubLocation.restore()
    spyIntervals.restore()
  })

  it('sends a message', () => {
    const textMessage = 'Some message'
    const messageToSend = { message: textMessage }
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().draw()
    httpSpy = HttpSpy.enableWith(messageToSend)
    page.fillMessage(textMessage)
    page.disableAskMessages()

    page.sendMessage()

    expect(httpSpy.hasBeenCalledWithMethod()).toEqual('POST')
    expect(httpSpy.hasBeenCalledWithUrl()).toContain('/store-message')
    expect(httpSpy.hasBeenCalledWithPayload()).toEqual(messageToSend)
  })

  it('cleans the message text input after send it', () => {
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().draw()
    page.fillMessage('someMessage')

    page.sendMessage()

    expect(page.hasMessageInputEmpty()).toEqual(true)
  })

  it('asks for messages at beginning', () => {
    const someMesage = 'Some text message'
    const anotherMessage = 'Another text message'
    const messages = { messages: [someMesage, anotherMessage], chat: "DEFAULT" }
    httpSpy = HttpSpy.enableWith(messages)

    const page = new TestLtfPage().draw()

    expect(httpSpy.hasBeenCalledWithMethod()).toEqual('GET')
    expect(httpSpy.hasBeenCalledWithUrl()).toContain('/retrieve-messages')
    expect(page.content()).toContain(someMesage)
    expect(page.content()).toContain(anotherMessage)
  })

  it('asks messages after send a message', () => {
    const someMesage = 'Some message'
    const messages = [someMesage]
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().draw()
    httpSpy = HttpSpy.enableWith({ messages })
    page.fillMessage(someMesage)

    page.sendMessage()

    expect(page.content()).toContain(someMesage)
  })

  it('does not send a empty message', () => {
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().draw()
    httpSpy = HttpSpy.enableWith(null)
    page.fillMessage('')
    page.disableAskMessages()

    page.sendMessage()

    expect(httpSpy.hasBeenCalled()).toEqual(false)
  })

  it('keeps the message text after draw the page', () => {
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().draw()
    page.fillMessage('someMessage')

    page.draw()

    expect(page.hasMessageInputEmpty()).toEqual(false)
  })

  it('asks for the messages recurrently', () => {
    httpSpy = HttpSpy.enableWith({ messages: [] })

    new TestLtfPage().draw()

    expect(spyIntervals.cleanedCallback()).toEqual('bound askMessages')
    expect(spyIntervals.executesCallbackEvery()).toEqual(5000)
    expect(spyIntervals.executes()).toEqual('bound askMessages')
  })

  it('sends a message pressing Enter key', () => {
    const message = 'Enter key presses'
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().draw()
    httpSpy = HttpSpy.enableWith({ messages: [message] })
    page.fillMessage(message)

    page.pressEnterAtMessage()

    expect(page.content()).toContain(message)
  })

  it('asks for chat messages', () => {
    const chat = 'someChatName'
    StubLocation.hash(chat)
    const messages = { messages: [], chat }
    httpSpy = HttpSpy.enableWith(messages)

    new TestLtfPage().draw()

    expect(httpSpy.hasBeenCalledWithUrl()).toContain('/retrieve-messages/' + chat)
  })

  it('sends a message for a chat', () => {
    const chat = 'someChatName'
    StubLocation.hash(chat)
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().draw()
    page.fillMessage('textMessage')
    page.disableAskMessages()

    page.sendMessage()

    expect(httpSpy.hasBeenCalledWithUrl()).toContain('/store-message/' + chat)
  })

  it('can open a new chat', () => {
    const spyWindow = new SpyWindow()
    const chatName = 'randomChatName'
    httpSpy = HttpSpy.enableWith({ messages: [] })
    const page = new TestLtfPage().withStubbedUuid(chatName).draw()

    page.clickNewChatButton()

    expect(spyWindow.hasOpenAnotherTab()).toEqual(true)
    expect(spyWindow.includesUrlOpenedTab(chatName)).toEqual(true)
    spyWindow.restore()
  })
})
