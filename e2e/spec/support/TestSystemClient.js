const redis = require('redis')

class TestSystemClient {
  static reset() {
    cy.exec('npm run reset-redis')
  }
}

module.exports = { TestSystemClient }
