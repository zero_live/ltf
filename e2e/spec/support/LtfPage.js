
class LtfPage {
  constructor() {
    cy.visit('/')
  }

  sendMessage(text) {
    cy.wait(500)
    cy.get('#message-text-input').clear({ force: true })
    cy.get('#message-text-input').type(text, { force: true })
    cy.get('#send-message-button').click()
  }

  includes(text) {
    cy.contains(text)

    return true
  }
}

module.exports = { LtfPage }
