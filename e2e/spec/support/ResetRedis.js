const redis = require('redis')

const client = redis.createClient('6379', 'redis')
client.flushdb(() => { client.end(true) })
