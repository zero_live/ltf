const { TestSystemClient } = require('./support/TestSystemClient')
const { LtfPage } = require('./support/LtfPage')

describe('LtfPage', () => {
  const someMessage = 'Some text message'
  const anotherMessage = 'Another text message'

  it('shows the sended messages', () => {
    TestSystemClient.reset()
    const page = new LtfPage()

    page.sendMessage(someMessage)
    page.sendMessage(anotherMessage)

    expect(page.includes(someMessage)).to.eq(true)
    expect(page.includes(anotherMessage)).to.eq(true)
  })

  it('keeps messages between sessions', () => {

    const page = new LtfPage()

    expect(page.includes(someMessage)).to.eq(true)
    expect(page.includes(anotherMessage)).to.eq(true)
  })
})
